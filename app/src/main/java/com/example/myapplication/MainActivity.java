package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    EditText text;
    Button delete;
    Button reestablish;
    ArrayList<Editable> currentText = new ArrayList<>();
    int AttemptsToRecover = 5;
    int i = 0, m = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = findViewById(R.id.text);
        delete = findViewById(R.id.delete);
        reestablish = findViewById(R.id.reestablish);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentText.add((Editable) text.getText());

                text.setText("");
                m = 1;
            }
        });

        reestablish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (m <= AttemptsToRecover) {
                    i = currentText.size() - m;
                    text.setText((String.format("%s", currentText.get(i))));
                    m++;
                } else {
                    Toast.makeText(MainActivity.this, "Восстановливать больше пяти раз невозможно!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}
